const lodash = require("lodash");

const env = process.env.NODE_ENV || "local";

const config = {
  env,
};

module.exports = lodash.assignIn(config, require(`./${config.env}`));
