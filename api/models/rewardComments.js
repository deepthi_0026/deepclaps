const { Model } = require("sequelize");

module.exports = (sequelize, DataTypes) => {
  class RewardComments extends Model {
    static associate(models) {
        RewardComments.belongsTo(models.rewards, {
        foreignKey: "reward_id",
      });
    }
  }
  RewardComments.init(
    {
      id: {
        field: "id",
        primaryKey: true,
        type: DataTypes.INTEGER,
        autoIncrement: true,
        allowNull: false,
      },
      rewardId: {
        field: "reward_id",
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "rewards",
          key: "id",
        },
      },
      comments: {
        field: "comments",
        type: DataTypes.STRING,
        allowNull: true,
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      }
    },
    {
      sequelize,
      modelName: "rewards_comments"
    }
  );
  return RewardComments;
};
